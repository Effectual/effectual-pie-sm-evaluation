<UCMDBId><xsl:value-of select="@id"/></UCMDBId>
<xsl:if test="@customer_id">
    <xsl:variable name="ucmdbCustomerId" select="@customer_id"/>
    <xsl:variable name="tenantMappingEntry" select="document('SM_MT_mapping.xml')/list['TenantMapping']/entry[@ucmdb=$ucmdbCustomerId]"/>
    <xsl:choose>
		<xsl:when test="$tenantMappingEntry">
            <CustomerId><xsl:value-of select="$tenantMappingEntry/@sm"/></CustomerId>					
        </xsl:when>
        <xsl:otherwise>
            <CustomerId><xsl:value-of select="@customer_id"/></CustomerId>
        </xsl:otherwise>
    </xsl:choose>
</xsl:if>
