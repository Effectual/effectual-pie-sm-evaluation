<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns="http://schemas.hp.com/SM/7">
	<xsl:template match="/ucmdbIDPushBack/ci">
		<model>
			<keys/>
			<instance>
				<xsl:for-each select="@id">
					<ConfigurationItem><xsl:value-of select="."/></ConfigurationItem>
				</xsl:for-each>
				<xsl:for-each select="@ucmdbid">
					<UcmdbID><xsl:value-of select="."/></UcmdbID>
				</xsl:for-each>
			</instance>
		</model>
	</xsl:template>
</xsl:stylesheet>
