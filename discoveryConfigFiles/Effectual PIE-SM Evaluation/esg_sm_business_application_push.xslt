<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/business_application">
        <model>
            <keys/>
            <instance>
                <!--import:cmdb_root_attributes.xslt-->
                <Type>bizservice</Type>
				<Subtype>Application Service</Subtype>
                <xsl:for-each select="@provider">
                    <ServiceProvider><xsl:value-of select="."/></ServiceProvider>
                </xsl:for-each>
                <xsl:for-each select="@display_label">
                    <ServiceName><xsl:value-of select="."/></ServiceName>
                    <CIIdentifier><xsl:value-of select="."/></CIIdentifier>
                </xsl:for-each>
            </instance>
        </model>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->