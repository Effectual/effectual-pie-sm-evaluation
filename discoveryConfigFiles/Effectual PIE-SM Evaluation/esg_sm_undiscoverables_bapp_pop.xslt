<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/RetrieveESG_ucmdbBusinessService_relatedListResponse">
        <topology>  
            <xsl:for-each select="instance">
                <ci class="business_application">
                    <xsl:for-each select="file.device">
                        <attribute name="name" type="String"><xsl:value-of select="CIIdentifier"/></attribute>
                        <attribute name="global_id" type="String"><xsl:value-of select="UCMDBId"/></attribute>
                        <attribute name="sm_id" type="String"><xsl:value-of select="CIName"/></attribute>                       
						<link direction="incoming" linkType="ownership">
                            <xsl:for-each select="Assignment">
                                <ci class="functional_group" ignoreCIIfEmpty="true">
                                    <attribute name="sm_id" type="string"><xsl:value-of select="."/></attribute>
                                    <attribute name="name" type="string"><xsl:value-of select="."/></attribute>
                                </ci>
                            </xsl:for-each>
                        </link>
                    </xsl:for-each>                         
                </ci>                  
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->