<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/RetrieveESG_ucmdbNodeListResponse">
        <topology>  
            <xsl:for-each select="instance">
                <xsl:variable name="smCIID" select="file.device/CIName"/>
                <ci class="unix" isRoot="true">                    
                    <xsl:for-each select="file.node">
                        <!--import:esg_sm_node_ips_mapping_population.xslt-->
                        <!--import:esg_sm_node_interfaces_mapping_population.xslt-->
                    </xsl:for-each>
                    <xsl:for-each select="file.device">
                        <!--import:esg_sm_node_sm_device_common_attributes_pop.xslt-->
                    </xsl:for-each>
               </ci>
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com
Version 1.4 / 2014-11-07 -->