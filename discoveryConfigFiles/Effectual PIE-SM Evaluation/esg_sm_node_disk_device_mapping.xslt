<xsl:for-each select="disk_devices">
    <disk.device>
        <xsl:for-each select="disk_device">
            <disk.device>        
                <ModelName><xsl:value-of select="@model_name"/></ModelName>
                <DiskVendor><xsl:value-of select="@vendor"/></DiskVendor>
                <DiskName><xsl:value-of select="@name"/></DiskName>
            </disk.device>
        </xsl:for-each>        
    </disk.device>
</xsl:for-each>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->