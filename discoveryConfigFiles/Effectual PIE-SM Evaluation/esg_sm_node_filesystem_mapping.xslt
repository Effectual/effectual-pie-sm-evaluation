<xsl:for-each select="file_systems">
    <file.system>
        <xsl:for-each select="file_system">
            <file.system>
                <MountPoint><xsl:value-of select="@mount_point"/></MountPoint>
                <FilesystemType><xsl:value-of select="@filesystem_type"/></FilesystemType>
                <DiskType><xsl:value-of select="@disk_type"/></DiskType>
                <DiskSize><xsl:value-of select="@disk_size"/></DiskSize>
            </file.system>
        </xsl:for-each>
    </file.system>
</xsl:for-each>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->