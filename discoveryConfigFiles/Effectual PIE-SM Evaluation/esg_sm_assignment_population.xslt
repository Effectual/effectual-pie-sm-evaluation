<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/RetrieveESG_ucmdbAssignmentListResponse">
        <topology>  
            <xsl:for-each select="instance">
                <ci class="functional_group">
                    <attribute name="sm_id" type="string"><xsl:value-of select="Assignment"/></attribute>
                    <attribute name="name" type="string"><xsl:value-of select="Assignment"/></attribute>
                    <attribute name="user_label" type="string"><xsl:value-of select="Assignment"/></attribute>
                    <!--link direction="incoming" linkType="membership">
                        <xsl:for-each select="Members">
                            <ci class="person">
                                <attribute name="name" type="string"><xsl:value-of select="."/></attribute>
                                <attribute name="sm_id" type="string"><xsl:value-of select="."/></attribute>
                                <attribute name="identification_key" type="string">.</attribute>
                            </ci>
                        </xsl:for-each>
                    </link-->
                </ci>                  
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25 -->