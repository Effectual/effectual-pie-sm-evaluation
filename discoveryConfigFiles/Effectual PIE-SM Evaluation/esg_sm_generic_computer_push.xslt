<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/host_node">
        <model>
            <keys/>
            <instance>
                <file.device>
                    <!--import:cmdb_root_attributes.xslt-->
                    <Type>computer</Type>
                    <xsl:variable name="prefix" select="'Value&gt;'"/>
                    <xsl:variable name="suffix" select="'&lt;/Value'"/>
                    <Subtype>Server</Subtype>
                    <xsl:if test="contains(@node_role,concat($prefix,'virtualized_system',$suffix))">
                        <IsVisualization>true</IsVisualization> 
                    </xsl:if>                    
                    <!--import:esg_sm_node_sm_device_common_attributes.xslt-->
                    <xsl:for-each select="@node_model">
                        <NodeModel><xsl:value-of select="."/></NodeModel>
                    </xsl:for-each>
                    <xsl:for-each select="organizations"> 
                        <xsl:for-each select="organization">
                            <CustomerId><xsl:value-of select="@name"/></CustomerId>
                            <Company><xsl:value-of select="@name"/></Company>
                        </xsl:for-each>
                    </xsl:for-each> 
                    <xsl:for-each select="locations">
                        <xsl:for-each select="location">
                            <Location><xsl:value-of select="@name"/></Location>            
                        </xsl:for-each>
                    </xsl:for-each>
                </file.device>
            </instance>
        </model>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->