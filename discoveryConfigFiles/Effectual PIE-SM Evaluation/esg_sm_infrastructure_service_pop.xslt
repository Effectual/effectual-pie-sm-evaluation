<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/RetrieveESG_ucmdbBusinessServiceListResponse">
        <topology>  
            <xsl:for-each select="instance">
                <ci class="infrastructure_service">
                    <xsl:for-each select="file.device">
                        <attribute name="name" type="String"><xsl:value-of select="CIIdentifier"/></attribute>
                        <attribute name="sm_id" type="String"><xsl:value-of select="CIName"/></attribute>
                        <attribute name="description" type="string"><xsl:value-of select="Description"/></attribute>
                    </xsl:for-each>                         
                </ci>                  
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->