<link direction="outgoing" linkType="containment">
    <xsl:for-each select="addlIPAddr/addlIPAddr">
        <ci class="ip_address">
            <attribute name="name" type="IPAddress" ignoreCIIfEmpty="true"><xsl:value-of select="AddlIPAddress"/></attribute>
            <attribute name="ip_netmask" type="IPAddress"><xsl:value-of select="AddlSubnet"/></attribute>
            <attribute name="sm_id" type="String"><xsl:value-of select="AddlIPAddress"/></attribute>
            <xsl:variable name="ipName" select="AddlIPAddress"/>
            <!--attribute name="sm_id" type="String"><xsl:value-of select="concat($smCIID, '_IP_', $ipName)"/></attribute-->
        </ci>
    </xsl:for-each>
</link>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->