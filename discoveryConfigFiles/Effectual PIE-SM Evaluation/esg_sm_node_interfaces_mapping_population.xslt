<link direction="outgoing" linkType="composition">
    <xsl:for-each select="AddlMacAddress/AddlMacAddress">
        <ci class="interface">
            <attribute name="name" type="String" ignoreCIIfEmpty="true"><xsl:value-of select="."/></attribute>
            <attribute name="mac_address" type="String"><xsl:value-of select="."/></attribute>
            <!--attribute name="sm_id" type="String"><xsl:value-of select="."/></attribute-->
            <xsl:variable name="interfaceName" select="."/>
            <attribute name="sm_id" type="String"><xsl:value-of select="concat($smCIID, '_interface_', $interfaceName)"/></attribute>
        </ci>
    </xsl:for-each>
</link>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->