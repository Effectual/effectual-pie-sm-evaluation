<link direction="outgoing" linkType="composition">
    <xsl:for-each select="disk.device/disk.device">
        <ci class="disk_device">
            <attribute name="name" type="String" ignoreCIIfEmpty="true"><xsl:value-of select="DiskName"/></attribute>
            <attribute name="model_name" type="String"><xsl:value-of select="ModelName"/></attribute>
            <attribute name="vendor" type="String"><xsl:value-of select="DiskVendor"/></attribute>
            <!--attribute name="sm_id" type="String"><xsl:value-of select="DiskName"/></attribute-->
            <xsl:variable name="diskDeviceName" select="DiskName"/>
            <attribute name="sm_id" type="String"><xsl:value-of select="concat($smCIID, '_diskdevice_', $diskDeviceName)"/></attribute>
        </ci>
    </xsl:for-each>
</link>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->