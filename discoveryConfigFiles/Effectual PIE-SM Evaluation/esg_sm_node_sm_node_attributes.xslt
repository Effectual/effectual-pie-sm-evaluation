<xsl:for-each select="@name">
    <HostName>
        <xsl:value-of select="."/>
    </HostName>
</xsl:for-each>
<xsl:for-each select="@discovered_os_version">
    <OSVersion>
        <xsl:value-of select="."/>
    </OSVersion>
</xsl:for-each>
<xsl:for-each select="@memory_size">
    <PhysicalMemory>
        <xsl:value-of select="."/>
    </PhysicalMemory>
</xsl:for-each>
<xsl:for-each select="@bios_serial_number">
    <BIOSId>
        <xsl:value-of select="."/>
    </BIOSId>    
</xsl:for-each>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->