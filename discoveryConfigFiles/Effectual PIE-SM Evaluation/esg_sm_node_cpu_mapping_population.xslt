<link direction="outgoing" linkType="composition">
    <xsl:for-each select="cpu/cpu">
        <ci class="cpu">
            <attribute name="cpu_id" type="String" ignoreCIIfEmpty="true"><xsl:value-of select="CpuID"/></attribute>
            <attribute name="name" type="String"><xsl:value-of select="CpuName"/></attribute>
            <attribute name="cpu_clock_speed" type="Long"><xsl:value-of select="CpuClockSpeed"/></attribute>
            <!--attribute name="sm_id" type="String"><xsl:value-of select="CpuName"/></attribute-->
            <xsl:variable name="cpuId" select="CpuID"/>
            <attribute name="sm_id" type="String"><xsl:value-of select="concat($smCIID, '_cpu_', $cpuId)"/></attribute>
        </ci>
    </xsl:for-each>
</link>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->