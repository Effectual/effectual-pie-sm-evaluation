<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/Retrievecirelationship1to1ListResponse">
        <topology>
            <xsl:for-each select="instance">              
               <ci class="object">
                    <attribute name="sm_id" type="String"><xsl:value-of select="upstreamci.id"/></attribute>
                    <attribute name="global_id" type="String"><xsl:value-of select="upstreamci.ucmdb.id"/></attribute>
                    <link direction="outgoing" linkType="containment">
                        <ci class="object">
                            <attribute name="sm_id" type="String"><xsl:value-of select="downstreamci.id"/></attribute>
                            <attribute name="global_id" type="String"><xsl:value-of select="downstreamci.ucmdb.id"/></attribute>
                        </ci>
                    </link>
               </ci>
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->