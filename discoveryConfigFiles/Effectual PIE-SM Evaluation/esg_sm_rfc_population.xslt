<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/RetrieveESG_UcmdbChangeListResponse">
        <topology>  
            <xsl:for-each select="instance">
                <ci class="request_for_change">
                    <xsl:for-each select="header">
						<attribute name="name" type="string"><xsl:value-of select="ChangeNumber"/></attribute>
						<attribute name="sm_id" type="string"><xsl:value-of select="ChangeNumber"/></attribute>
						<attribute name="reference_number" type="string"><xsl:value-of select="ChangeNumber"/></attribute>
						<attribute name="description" type="string"><xsl:value-of select="BriefDescription"/></attribute>
						<attribute name="planned_end_date" type="date"><xsl:value-of select="PlannedEndDate"/></attribute>
						<attribute name="planned_start_date" type="date"><xsl:value-of select="PlannedStartDate"/></attribute>                        
						<attribute name="rfc_status" type="string"><xsl:value-of select="Status"/></attribute>
						<attribute name="approval_status" type="string"><xsl:value-of select="ApprovalStatus"/></attribute>                 
						<!--Priority-->
						<xsl:variable name="ci_priority"><xsl:value-of select="Priority"/></xsl:variable>
						<attribute name="priority" type="string">
							<xsl:if test="$ci_priority = '1'">1_critical</xsl:if>
							<xsl:if test="$ci_priority = '2'">2_high</xsl:if>
							<xsl:if test="$ci_priority = '3'">3_average</xsl:if>
							<xsl:if test="$ci_priority = '4'">4_low</xsl:if>
						</attribute>
						<!--RiskAssessment-->
						<xsl:variable name="rfc_risk_assessment"><xsl:value-of select="RiskAssessment"/></xsl:variable>
						<attribute name="risk_assessment" type="string">
							<xsl:if test="$rfc_risk_assessment = '0'">no_risk</xsl:if>
							<xsl:if test="$rfc_risk_assessment = '1'">low_risk</xsl:if>
							<xsl:if test="$rfc_risk_assessment = '2'">some_risk</xsl:if>
							<xsl:if test="$rfc_risk_assessment = '3'">moderate_risk</xsl:if>
							<xsl:if test="$rfc_risk_assessment = '4'">high_risk</xsl:if>
							<xsl:if test="$rfc_risk_assessment = '5'">very_high_risk</xsl:if>
						</attribute>
						<!--ChangeCategory-->
						<xsl:variable name="rfc_change_category"><xsl:value-of select="Category"/></xsl:variable>
						<attribute name="change_category" type="string">
							<xsl:if test="$rfc_change_category = 'CI Group'">ci_group</xsl:if>
							<xsl:if test="$rfc_change_category = 'Hardware'">hardware</xsl:if>
							<xsl:if test="$rfc_change_category = 'Maintenance'">maintenance</xsl:if>
							<xsl:if test="$rfc_change_category = 'Network'">network</xsl:if>
							<xsl:if test="$rfc_change_category = 'Software'">software</xsl:if>
							<xsl:if test="$rfc_change_category = 'Emergency Change'">emergency_rfc</xsl:if>
							<xsl:if test="$rfc_change_category = 'Normal Change'">normal_change</xsl:if>
							<xsl:if test="$rfc_change_category = 'Standard Change'">standard_change</xsl:if>                           
						</attribute>
						<!--ChangePhase-->
						<xsl:variable name="rfc_change_phase"><xsl:value-of select="CurrentPhase"/></xsl:variable>
						<attribute name="change_phase" type="string">
							<xsl:if test="$rfc_change_phase = 'Change Logging'">logging</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Change Review'">review</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Change Assessment &amp; Planning'">assessment</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Prepare for Change Approval'">prepare_for_approval</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Change Approval'">approval</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Change Implementation'">implementation</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Evaluation &amp; Change Closure'">evaluation</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Registration and Categorization'">registration_and_categorization</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Risk and Impact Analysis'">risk_and_impactanalysis</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Abandoned'">abandoned</xsl:if>
							<xsl:if test="$rfc_change_phase = 'ECAB Approval'">ecab_approval</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Build and Test'">build_and_test</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Implementation'">implement</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Backout'">back_out</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Post Implementation Review'">post_implement_review</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Closure'">closure</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Validation'">validation</xsl:if>
							<xsl:if test="$rfc_change_phase = 'TCAB Approva'">tcab_approval</xsl:if>
							<xsl:if test="$rfc_change_phase = 'DCAB Approval'">dcab_approval</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Deployment'">deployment</xsl:if>
							<xsl:if test="$rfc_change_phase = 'CMDB Update'">cmdb_update</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Plan and Schedule'">plan_and_schedule</xsl:if>
							<xsl:if test="$rfc_change_phase = 'Execution'">execution</xsl:if>
						</attribute>
                    </xsl:for-each>
                    <!--ImpactScope-->
                    <xsl:variable name="ci_impact_scope"><xsl:value-of select="InitialAssessment"/></xsl:variable>
                    <attribute name="impact_scope" type="string">
                        <xsl:if test="$ci_impact_scope = '1'">enterprise</xsl:if>
                        <xsl:if test="$ci_impact_scope = '2'">site_dept</xsl:if>
                        <xsl:if test="$ci_impact_scope = '3'">multiple_users</xsl:if>
                        <xsl:if test="$ci_impact_scope = '4'">user</xsl:if>
                    </attribute>
                    <!--Urgency-->
                    <xsl:variable name="ci_urgency"><xsl:value-of select="Urgency"/></xsl:variable>
                    <attribute name="urgency" type="string">
                        <xsl:if test="$ci_urgency = '1'">1_critical</xsl:if>
                        <xsl:if test="$ci_urgency = '2'">2_high</xsl:if>
                        <xsl:if test="$ci_urgency = '3'">3_average</xsl:if>
                        <xsl:if test="$ci_urgency = '4'">4_low</xsl:if>
                    </attribute>
                </ci>                  
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->