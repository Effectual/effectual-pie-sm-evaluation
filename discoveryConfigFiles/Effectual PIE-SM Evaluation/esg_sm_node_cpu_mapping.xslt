<xsl:for-each select="cpus">
    <cpu>
        <xsl:for-each select="cpu">
            <cpu>
                <CpuID><xsl:value-of select="@cpu_id"/></CpuID>
                <CpuName><xsl:value-of select="@name"/></CpuName>
                <CpuClockSpeed><xsl:value-of select="@cpu_clock_speed"/></CpuClockSpeed>
            </cpu>
        </xsl:for-each>
    </cpu>
</xsl:for-each>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->