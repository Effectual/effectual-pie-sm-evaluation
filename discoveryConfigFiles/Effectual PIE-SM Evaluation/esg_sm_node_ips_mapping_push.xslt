<xsl:for-each select="ip_addresss">
    <addlIPAddr>
        <xsl:for-each select="ip_address">
            <addlIPAddr>
                <AddlIPAddress><xsl:value-of select="@name"/></AddlIPAddress>
                <AddlSubnet><xsl:value-of select="@ip_netmask"/></AddlSubnet>
            </addlIPAddr>
        </xsl:for-each>
    </addlIPAddr>
</xsl:for-each>
<!--XSLT for use with the Integration By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.3 / 2014-07-25
Original Syntax based on the HP Adapter -->