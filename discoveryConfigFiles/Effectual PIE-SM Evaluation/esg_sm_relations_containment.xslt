<xsl:stylesheet version='1.0'  xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:template match="/relation">
        <model>
            <keys/>
            <instance>
                <ParentCI><xsl:value-of select="@parentID"/></ParentCI>
                <ChildCIs>
                    <ChildCIs><xsl:value-of select="@childID"/></ChildCIs>
                </ChildCIs>
                <RelationshipType>Containment</RelationshipType>
            </instance>
        </model>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->