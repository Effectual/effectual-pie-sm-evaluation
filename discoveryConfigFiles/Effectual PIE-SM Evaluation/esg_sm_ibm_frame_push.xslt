<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/ibm_pseries_frame">
        <model>
            <keys/>
            <instance>
                <file.device>
                    <!--import:cmdb_root_attributes.xslt-->
                    <Type>computer</Type>
                    <Subtype>Virtualized System</Subtype>
                    <xsl:variable name="prefix" select="'Value&gt;'"/>
                    <xsl:variable name="suffix" select="'&lt;/Value'"/>
                    <xsl:if test="contains(@node_role,concat($prefix,'virtualized_system',$suffix))">
                        <IsVisualization>true</IsVisualization> 
                    </xsl:if>                    
                    <!--import:esg_sm_node_sm_device_common_attributes.xslt-->
                    
                </file.device>
                <file.node>
                    <!--import:esg_sm_node_sm_node_attributes.xslt-->
                    <!--import:esg_sm_node_cpu_mapping.xslt-->
                </file.node>
            </instance>
        </model>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->