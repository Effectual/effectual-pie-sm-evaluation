<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/lpar">
        <model>
            <keys/>
            <instance>
                <file.device>
                    <!--import:cmdb_root_attributes.xslt-->
                    <Type>mainframe</Type>
                    <Subtype>Logical Partition</Subtype>                    
                    <!--import:esg_sm_node_sm_device_common_attributes.xslt-->
                    
                </file.device>
                <file.node>
                    <!--import:esg_sm_node_sm_node_attributes.xslt-->
                    <!--import:esg_sm_node_cpu_mapping.xslt-->
                    <!--import:esg_sm_node_disk_device_mapping.xslt-->
                    <!--import:esg_sm_node_filesystem_mapping.xslt-->
                    <!--import:esg_sm_node_ips_mapping_push.xslt-->
                    <!--import:esg_sm_node_interfaces_mapping_push.xslt-->
                </file.node>
            </instance>
        </model>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->