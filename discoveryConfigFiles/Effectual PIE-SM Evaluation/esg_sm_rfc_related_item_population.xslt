<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/RetrieveESG_UcmdbChangeListResponse">
        <topology>  
            <xsl:for-each select="instance">
                <xsl:variable name="ServiceId"><xsl:value-of select="middle/AffectedServiceGuid"/></xsl:variable>
                <ci class="request_for_change">
                    <xsl:for-each select="header">						
                        <attribute name="reference_number" type="string"><xsl:value-of select="ChangeNumber"/></attribute>
                        <attribute name="sm_id" type="string"><xsl:value-of select="ChangeNumber"/></attribute>
						<link direction="incoming" linkType="responsibility">
							<xsl:for-each select="Assignment">
								<ci class="functional_group">
									<attribute name="name" type="string"><xsl:value-of select="."/></attribute>
									<attribute name="sm_id" type="string"><xsl:value-of select="."/></attribute>
								</ci>
							</xsl:for-each>
						</link>
                    </xsl:for-each>
                    <xsl:for-each select="middle">
                        <xsl:if test="not(not(normalize-space(AffectedCIGuid)))">
                            <xsl:for-each select="AffectedCIGuid">
                                <xsl:for-each select="AffectedCIGuid">
                                    <link direction="outgoing" linkType="connection">
                                        <ci class="object" ignoreifempty="true">
                                            <attribute name="sm_id" type="string"><xsl:value-of select="."/></attribute>
                                            <attribute name="global_id" type="string"><xsl:value-of select="."/></attribute>
                                        </ci>
                                    </link> 
                                </xsl:for-each> 
                            </xsl:for-each>
                        </xsl:if> 
						<xsl:for-each select="AffectedServiceGuid">
							<link direction="outgoing" linkType="connection">
								<ci class="object">
									<attribute name="sm_id" type="string"><xsl:value-of select="."/></attribute>
									<attribute name="global_id" type="string"><xsl:value-of select="."/></attribute>
								</ci>
							</link>
						</xsl:for-each>						
                    </xsl:for-each>
                </ci>                  
            </xsl:for-each>
        </topology>
    </xsl:template>
</xsl:stylesheet>
<!--XSLT Developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->