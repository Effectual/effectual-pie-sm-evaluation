<attribute name="name" type="String"><xsl:value-of select="CIIdentifier"/></attribute>
<attribute name="sm_id" type="String"><xsl:value-of select="CIName"/></attribute>
<attribute name="serial_number" type="String"><xsl:value-of select="SerialNo"/></attribute>
<attribute name="memory_size" type="String"><xsl:value-of select="PhysicalMemory"/></attribute>
<attribute name="discovered_os_name" type="String"><xsl:value-of select="OS"/></attribute>
<attribute name="default_gateway_ip_address" type="String"><xsl:value-of select="DefaultGateway"/></attribute>
<attribute name="primary_dns_name" type="String"><xsl:value-of select="DNSName"/></attribute>
<attribute name="discovered_vendor" type="String"><xsl:value-of select="Vendor"/></attribute>