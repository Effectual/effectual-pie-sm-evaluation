<xsl:for-each select="@default_gateway_ip_address">
    <DefaultGateway><xsl:value-of select="."/></DefaultGateway>
</xsl:for-each>
<xsl:for-each select="@discovered_os_name">
    <OS><xsl:value-of select="."/></OS>
</xsl:for-each>
<xsl:for-each select="@primary_dns_name">
    <DNSName>
        <xsl:value-of select="."/>
    </DNSName>
</xsl:for-each>                    
<xsl:for-each select="@serial_number">
    <SerialNo>
        <xsl:value-of select="."/>
    </SerialNo>
</xsl:for-each>
<xsl:for-each select="@discovered_vendor">
    <Vendor>
        <xsl:value-of select="."/>
    </Vendor>
</xsl:for-each>
<addl>
	<addl>
		<AddlName><xsl:value-of select="@name"/></AddlName>
		<AddlType><xsl:value-of select="@root_class"/></AddlType>			
	</addl>
</addl>
<!-- For use with the Service Manager Adapter developed By: Effectual Systems Group, Inc.
1911 Addison Street, Suite 203, Berkeley, CA 94704 USA
+1-510-323-2240 / Contact@ef-sy.com 
Version 1.4 / 2014-11-07 -->